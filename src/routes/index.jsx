import AddEditHomePage from "../modules/views/AddEditHomePage.jsx"

var indexRoutes = [
  { path: "/", name: "post", component: AddEditHomePage }
];

export default indexRoutes;
