import $ from 'jquery';
let Script2=(
      $('#review').owlCarousel({
          items:3,
          loop:true,
          margin:10,
          rtl:false,
          dots:false,
          autoplay: false,
          navigation:true,
          slideTransition:'linear',
          autoplayTimeout: 0,
          autoplaySpeed: 50000,
          navigationText: ["<i class='fas fa-chevron-left'></i>","<i class='fas fa-chevron-right'></i>"],
          
          responsive:{
              0:{
                  items:1
              },
              600:{
                  items:2
              },
              1000:{
                  items:3
              }
          }


      })
)

module.exports = {
    Script2
} 
      
