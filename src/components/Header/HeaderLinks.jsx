import React, { Component } from "react";
import cookie from 'react-cookies';
import { NavLink } from "react-router-dom";
import axios from 'axios'
import Moment from "moment";
import { Nav, NavDropdown, MenuItem, Modal } from "react-bootstrap";

import ReimApplyModal from "modules/hrms/reimbursements/reim_data/components/ReimApplyModal.jsx";
import LeaveApplyModal from "modules/hrms/leaves/leave_data/components/LeaveApplyModal.jsx";
import InquiryFormComponent from "modules/sales/inquiries/components/InquiryFormComponent.jsx";
import MaintenanceLogComponents from "modules/assets/calendar/components/MaintenanceLogComponents.jsx";
import NewPaymentModal from "modules/finance/payments/components/NewPaymentModal.jsx";
import MrFormComponent from "modules/inventory/materials/materialrequest/components/MrFormComponent.jsx";
import LeadsKanbanComponent from "modules/crm/leads/components/LeadsKanbanComponent.jsx";
import EditProfilePage from "views/EditProfile/views/EditProfilePage.jsx"
import ChangePasswordPage from "views/ChangePassword/views/ChangePasswordPage.jsx"

import { backendURL } from 'variables/appVariables.jsx';
import help from "assets/img/help.png";

class HeaderLinksPage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      availableMaintenances: null,
      maintenanceDate: null,
      showInquiryModal: false,
      showLeadsModal: false,
      showMrModal: false,
      maintenanceList: [],
      header: {
        notifications: {
          count: 0,
          data: []
        },
        maintenances: {
          count: 0,
          data: []
        },
      }
    }

  }

  componentWillMount() {
    this.fetchDataFromServer();
  }

  fetchDataFromServer = () => {
    // var _this = this
    axios.get(backendURL + "dashboard")
      .then(function (res) {
      })

  }

  createTableData = () => {
    var tableRows = [];
    return tableRows;
  }

  logout = () => {
    cookie.remove('user');
    cookie.remove('role');
    cookie.remove('code')
    cookie.remove('email');
    cookie.remove('username');
    cookie.remove('picture');
    cookie.remove('token');
  }
  render() {
    let userName = (cookie.load('user'))

    var maintanceLogMOdal = (
      <Modal
        dialogClassName="large-modal"
        show={this.state.showMaintanceLogModal}
        onHide={() => this.setState({ showMaintanceLogModal: false })}>
        <Modal.Header class="header1" >
          <div className="modal-close">
            <a role="button" className="fa fa-times text-danger" fill pullRight icon onClick={() => this.setState({ showMaintanceLogModal: false })}>{null}</a>
          </div>
          <Modal.Title>Add/Edit Maintenance Log</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <MaintenanceLogComponents
            {...this.props}
            showMaintanceLogModal={() => this.setState({ showMaintanceLogModal: true })}
            handleCloseMaintanceLogModal={() => this.setState({ showMaintanceLogModal: false })}
          />
        </Modal.Body>
      </Modal>
    );
    var inquiryModal = (
      <Modal
        dialogClassName="large-modal"
        show={this.state.showInquiryModal}>
        <Modal.Header class="header1">
          <div className="modal-close">
            <a role="button"
              className="fa fa-times text-danger" fill pullRight icon
              onClick={() => this.setState({ showInquiryModal: false })}
            >{null}</a>
          </div>
          <Modal.Title>Add Inquiry</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <InquiryFormComponent
            isFromHeader
            handleShowInquiryModal={() => this.setState({ showInquiryModal: true })}
            handleCloseInquiryModal={() => this.setState({ showInquiryModal: false })}
            createInquiry={true}
            {...this.props} />
        </Modal.Body>
      </Modal>
    );
    var leaveModal = (
      <Modal
        show={this.state.showLeaveModal}>
        <Modal.Header class="header1">
          <div className="modal-close">
            <a role="button"
              className="fa fa-times text-danger" fill pullRight icon
              onClick={() => this.setState({ showLeaveModal: false })}
            >{null}</a>
          </div>
          <Modal.Title>Apply Leave</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <LeaveApplyModal
            {...this.props}
            closeLeaveModal={() => this.setState({ showLeaveModal: null })}
          />
        </Modal.Body>
      </Modal>
    );
    var materialRequestModal = (
      <Modal
        dialogClassName="large-modal"
        show={this.state.showMrModal}
      >
        <Modal.Header class="header1">
          <div className="modal-close">
            <a role="button"
              className="fa fa-times text-danger" fill pullRight icon
              onClick={() => this.setState({ showMrModal: false })}
            >{null}</a>
          </div>
          <Modal.Title>Create Material Request</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <MrFormComponent
            {...this.props}
            view="create-mr-wo-bom"
            closeModal={() => this.setState({ showMrModal: false })}
          />
        </Modal.Body>
      </Modal>
    )
    var paymentModal = (
      <Modal
        show={this.state.showPaymentModal}>
        <Modal.Header class="header1" >
          <div className="modal-close">
            <a role="button"
              className="fa fa-times text-danger" fill pullRight icon
              onClick={() => this.setState({ showPaymentModal: false })}
            >{null}</a>
          </div>
          <Modal.Title>Create New Payment</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <NewPaymentModal
            handleShowPaymentModal={() => this.setState({ showPaymentModal: true })}
            handleClosePaymentModal={() => this.setState({ showPaymentModal: false })}
            createPayment={true}
            {...this.props} />
        </Modal.Body>
      </Modal>
    );
    var EditProfileModal = (
      <Modal
        dialogClassName="large-modal"
        show={this.state.showUserProfileModal}>
        <Modal.Header class="header1">
          <div className="modal-close">
            <a role="button"
              className="fa fa-times text-danger" fill pullRight icon
              onClick={() => this.setState({ showUserProfileModal: false })}
            >{null}</a>
          </div>
          <Modal.Title>View Profile </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <EditProfilePage
            isFromHeader
            handleCloseUserProfileModal={() => this.setState({ showUserProfileModal: false })}
            {...this.props} />
        </Modal.Body>
      </Modal>
    );
    var changePasswordModal = (
      <Modal
        //  dialogClassName="large-modal"
        show={this.state.showChangePasswordModal}>
        <Modal.Header class="header1">
          <div className="modal-close">
            <a role="button"
              className="fa fa-times text-danger" fill pullRight icon
              onClick={() => this.setState({ showChangePasswordModal: false })}
            >{null}</a>
          </div>
          <Modal.Title>Change Password</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <ChangePasswordPage
            isFromHeader
            handleshowChangePasswordModalModal={() => this.setState({ showChangePasswordModal: false })}
            {...this.props} />
        </Modal.Body>
      </Modal>
    );
    var reimModal = (
      <Modal
        show={this.state.showReimModal}>
        <Modal.Header class="header1">
          <div className="modal-close">
            <a role="button"
              className="fa fa-times text-danger" fill pullRight icon
              onClick={() => this.setState({ showReimModal: false })}
            >{null}</a>
          </div>
          <Modal.Title>Apply Reimbursement</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <ReimApplyModal
            {...this.props}
            closeModal={() => this.setState({ showReimModal: null })}
          />
        </Modal.Body>
      </Modal>
    );
    var leadsModal = (
      this.state.showLeadsModal
        ? (
          <LeadsKanbanComponent
            showLeadsModal={this.state.showLeadsModal}
            handleCloseLeadsModal={() => this.setState({ showLeadsModal: false })}
            createLead={true}
            isShowFiled={true}
            {...this.props} />
        ) : null
    );

    return (
      <div>
        {maintanceLogMOdal}
        {paymentModal}
        {inquiryModal}
        {leaveModal}
        {materialRequestModal}
        {EditProfileModal}
        {changePasswordModal}
        {leadsModal}
        {reimModal}
        <Nav pullRight>
          <NavDropdown eventKey={2}
            title={
              <div>
                <img src={help} alt="" style={{ height: "25px", width: "25px", borderRadius: "25px", marginTop: "13px" }} />
              </div>
            }
            noCaret
            id="basic-nav-dropdown-1">
            <MenuItem eventKey={2.1}><i className="fa fa-phone" /> + 917887886462 </MenuItem>
            <MenuItem eventKey={2}><i className="fa fa-envelope" /> support@arrowsofterp.com</MenuItem>
          </NavDropdown>
          <NavDropdown eventKey={3}
            title={
              <div>
                <i className="fa fa-wrench" style={{ font: "25px" }} />
                <span className="notification">{this.state.availableMaintenances}</span>
                <p className="hidden-md hidden-lg">
                  maintenance log
                  <b className="caret" />
                </p>
              </div>
            }
            noCaret
            id="basic-nav-dropdown-2"
          >
            {
              !this.state.maintenanceList || !this.state.maintenanceList.length
                ? (<MenuItem eventKey={3.1}>No maintenance log found.</MenuItem>)
                : (
                  this.state.maintenanceList.map((prop, key) => {
                    return (
                      <MenuItem eventKey={3.1}>{key + 1}>
                        <i onClick={() => this.handleShowMaintanceLogModal(prop.code)} />
                        {prop.maintenanceDate ? Moment(prop.maintenanceDate).format("DD-MMM-YYYY") : null}-{prop.asset.name ? prop.asset.name : ""}
                      </MenuItem>
                    )
                  })
                )
            }
          </NavDropdown>
          <NavDropdown eventKey={4}
            title={
              <div>
                <i className="fa fa-bell-o" />
                <span className="notification">{this.state.header.notifications.count}</span>
                <p className="hidden-md hidden-lg">
                  Notifications
                  <b className="caret" />
                </p>
              </div>
            }
            noCaret
            id="basic-nav-dropdown-2"
          >
            <MenuItem eventKey={4.1}></MenuItem>
          </NavDropdown>
          <NavDropdown eventKey={5}
            title={
              <div>
                <i className="fa fa-shopping-cart" />
                <p className="hidden-md hidden-lg">
                  More
                  <b className="caret" />
                </p>
              </div>
            }
            noCaret
            id="basic-nav-dropdown-3"
            bsClass="dropdown-with-icons dropdown"
          >
            <MenuItem eventKey={5.1} onClick={() => this.setState({ showLeadsModal: true })}><i className="fa fa-plus" /> Lead</MenuItem>
            <MenuItem eventKey={5.2} onClick={() => this.setState({ showInquiryModal: true })}><i className="fa fa-plus" /> Inquiry</MenuItem>
          </NavDropdown>
          <NavDropdown eventKey={6}
            title={
              <div>
                <i className="fa fa-gavel" />
                <p className="hidden-md hidden-lg">
                  More
                  <b className="caret" />
                </p>
              </div>
            }
            noCaret
            id="basic-nav-dropdown-3"
            bsClass="dropdown-with-icons dropdown"
          >
            <MenuItem eventKey={6.1} onClick={() => this.setState({ showReimModal: true })}><i className="fa fa-plus" /> Reimbursement</MenuItem>
            <MenuItem eventKey={6.2} onClick={() => this.setState({ showMrModal: true })}><i className="fa fa-plus" /> Material Request</MenuItem>
          </NavDropdown>
          <NavDropdown eventKey={7}
            title={
              <div>
                <i className="fa fa-users" />
                <p className="hidden-md hidden-lg">
                  More
                  <b className="caret" />
                </p>
              </div>
            }
            noCaret
            id="basic-nav-dropdown-3"
            bsClass="dropdown-with-icons dropdown"
          >
            <MenuItem eventKey={7.1} onClick={() => this.setState({ showLeaveModal: true })}><i className="fa fa-plus" /> Apply Leave</MenuItem>
          </NavDropdown>
          <NavDropdown className="user" eventKey={10} style={{ paddingTop: "0px" }}
            title={
              <div>
                {cookie.load('user') ? cookie.load('user') : "USERNAME"}<i className="fa fa-caret-down"></i>
              </div>
              // <div className="photo">
              //   <img src={avatar} alt="Avatar" style={{ height: "25px", width: "25px", borderRadius: "25px" }} />
              // </div>

              // for Login user photo

              // <div className="photo">
              //   {cookie.load('picture') ?
              //     <img src={backendURL + cookie.load('picture')} alt="" style={{ height: "25px", width: "25px", borderRadius: "25px" }} />
              //     :
              //     <img src={avatar} alt="" style={{ height: "25px", width: "25px", borderRadius: "25px" }} />
              //   }
              // </div>
            }
            noCaret
            id="basic-nav-dropdown-3"
            bsClass="dropdown-with-icons dropdown">
            <MenuItem eventKey={10.1} onClick={() => this.setState({ showUserProfileModal: true })} >
              <i className="fa fa-user" />
              {
                userName && userName.length > 20
                  ? (
                    cookie.load('user')
                      ? (
                        cookie.load('user')
                      ) : (
                        "USERNAME").substring(0, 20) + "...."
                  ) : (
                    cookie.load('user')
                      ? (
                        cookie.load('user')
                      ) : (
                        "USERNAME")
                  )
              }
              <NavLink to={"/settings/user-edit/"} className="nav-link" activeClassName="active"> </NavLink>
            </MenuItem>
            <MenuItem eventKey={10.2} >
              <i className="fa fa-male" href="#" /> Employee id: {cookie.load('username') ? cookie.load('username') : null}
            </MenuItem>

            <MenuItem eventKey={10.3} ><i className="fa fa-male" href="#" />
              Role:
              {
                cookie.load('role') && cookie.load('role').length > 20
                  ? (
                    (cookie.load('role') ? cookie.load('role') : "USERNAME").substring(0, 20) + "...."
                  ) : (
                    (cookie.load('role') ? cookie.load('role') : "USERNAME")
                  )
              }
              <NavLink to={"#"} className="nav-link" activeClassName="active">  </NavLink>
            </MenuItem>
            <MenuItem eventKey={10.4} > <i className="fa fa-lock" onClick={() => this.setState({ showChangePasswordModal: true })} />
              <NavLink to={"#"} className="nav-link" activeClassName="active" onClick={() => this.setState({ showChangePasswordModal: true })}>
                Change Password
               </NavLink>
            </MenuItem>
            <MenuItem divider />
            <MenuItem eventKey={10.5} href="#/" onClick={this.logout}> <i style={{ color: 'Red' }} className="fa fa-power-off fa-8x" />
              <NavLink style={{ color: 'Red' }} to="/" className="nav-link" activeClassName="active">
                Log out
              </NavLink>
            </MenuItem>
          </NavDropdown>
        </Nav>        
      </div>
    );
  }
}
export default HeaderLinksPage;
