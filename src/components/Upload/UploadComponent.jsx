/* eslint-disable no-unused-expressions */
import React from 'react';
import { Col } from 'react-bootstrap';
import CryptoJS from "crypto-js";
import axios from 'axios';
import cookie from 'react-cookies';
import SweetAlert from "react-bootstrap-sweetalert";

import Button from 'components/CustomButton/CustomButton.jsx';

import { backendURL, encdec } from 'variables/appVariables.jsx';
import { downloadFile } from "js/server.js";
// import { checkAccess } from "js/access.control.js";
import { successAlert } from "js/alerts";

class UploadComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      code: null,
      photo: props.photo,
      ImageUploader: false,
      documentUplader: false,
      completed: ""
    };

  }

  handleImageChange = (e) => {
    var url;
    if (this.props.type === "users") {
      url = `${backendURL}common/documents/${this.props.type}/picture/${(this.props.details.id ? this.props.details.id : "")}/${this.props.variable}`;
    } else {
      url = `${backendURL}common/documents/${this.props.type}/picture/${(this.props.details.code ? this.props.details.code : "")}/${this.props.variable}`;
    }
    e.preventDefault();
    let reader = new FileReader();
    let file = e.target.files[0];

    reader.onloadend = () => {
      if (!file.type.includes('image')) {
        alert('Please select an image file !')
      } else if (file.size / (1024 * 1024) > 1) {                //500 kb =0.5 and 1024 mb =1
        alert('Please select image smaller than 1 MB')
      } else {
        var ifd = new FormData();
        ifd.append('file', file)
        axios.post(url, ifd, { headers: { 'Authorization': 'Bearer ' + cookie.load('token') } }).then((res) => {
          if (res.status === 200) {
            setTimeout(() => {
              this.props.handleImageChange(res.data);
              this.setState({
                file: file,
                photo: backendURL + res.data
              });
            }, 100);
          }
        }).catch(function (error) {
          console.error(error.response);
        });
      }
    };
    !file ? null : reader.readAsDataURL(file);
  }

  handleDocumentChange = (e) => {
    var url = `${backendURL}common/documents/${this.props.type}/documents/${this.props.details.code ? this.props.details.code : null}/${this.props.variable}`;
    // if (this.props.details.code === "New") {
    //   this.props.details.code = null;
    //   url = backendURL + 'common/documents/' + this.props.type + '/documents/' + (this.props.details.code ? this.props.details.code : null);
    // } else {
    //   url = backendURL + 'common/documents/' + this.props.type + '/documents/' + (this.props.details.code ? this.props.details.code : null);
    // }
    e.preventDefault();
    for (let i = 0; i < e.target.files.length; i++) {
      let reader = new FileReader();
      let file = e.target.files[i];
      reader.onloadstart = () => {
        this.setState({ documentUplader: true })
      }
      reader.onloadend = () => {
        var _this = this;
        if (file.size / (1024 * 1024) > 1) {
          alert('Please choose document smaller than 1 MB')
        } else {
          // var url = url;
          var ifd = new FormData();
          ifd.append('file', file);
          axios({
            url: url, method: 'post',
            onUploadProgress: function (progressEvent) {
              var percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
              _this.setState({ completed: percentCompleted })
            }, headers: { 'Authorization': 'Bearer ' + cookie.load('token') }, data: ifd
          }).then((res) => {
            _this.setState({ documentUplader: false });
            _this.setState({ file: file });
            if (this.props.drawings) {
              _this.props.handleDocumentChange(res.data, _this.props._this);
            } else {
              var bytes = CryptoJS.AES.decrypt(res.data.toString(), encdec);
              var decryptedData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
              _this.props.handleDocumentChange(decryptedData);
            }
          }).catch(function (error) {
            console.error(error.response);
          });
        }
      }
      reader.readAsDataURL(file);
    }
  }

  deleteDocumentFromServer = (documentFileName, key) => {
    var url = `${backendURL}common/${documentFileName}/${this.props.variable === "kickoff_doc" ? "documents" : this.props.variable}?moduleId=${this.props.details.id}`;
    var _this = this;
    axios.delete(url, { 'headers': { 'Authorization': 'Bearer ' + cookie.load('token') } })
      .then((res) => {
        if (res.status === 200) {
          _this.props.handleDeleteDocument(this.props.variable === "kickoff_doc" ? _this.props.index : key);
          _this.setState({ alert: "", })
          successAlert("Document deleted successfully!", _this)
        }
      }).catch((error) => {
        console.error(error);
      });
  }

  deleteConfirmMessage = (documentFileName, key) => {
    this.setState({
      alert: (
        <SweetAlert
          warning
          style={{ display: "block", marginTop: "-100px" }}
          title="Are you sure?"
          onConfirm={() => this.deleteDocumentFromServer(documentFileName, key)}
          onCancel={() => this.hideAlert()}
          confirmBtnBsStyle="danger"
          cancelBtnBsStyle="info"
          confirmBtnText="Yes, delete it!"
          cancelBtnText="Cancel"
          showCancel
        >
          You will not be able to recover this document!
        </SweetAlert>
      )
    });
  }

  hideAlert = () => {
    this.setState({
      alert: null
    });
  }

  render() {
    var _this = this;
    return (
      <div>
        <div className="picture-container">
          {this.state.alert}
          <div className={this.props.picture ? "picture" : null}>
            {
              this.props.picture
                ? (
                  <div className="parent-picture-src" key={this.props.photo} >
                    {/* <img src={`${backendURL}common/${this.props.photo}`} className="picture-src" alt="..." />
                    <input type="file" onChange={(e) => this.handleImageChange(e)} /> */}
                    <label for="picture_input" style={{ cursor: "pointer" }}>
                      <img src={`${backendURL}common/${this.props.photo}`} className="picture-src" alt="..." />
                      <input type="file" id="picture_input" style={{ display: "none" }} onChange={(e) => this.handleImageChange(e)} />
                    </label>
                  </div>
                ) : null
            }
          </div>
        </div>
        <div>
          {
            this.props.document
              ? (
                <div key={this.props.documents}>
                  {
                    _this.props.variable === "kickoff_doc" && _this.props.documents !== null
                      ? (
                        <Col xs={12} sm={_this.props.smallModal ? 12 : 6} md={_this.props.smallModal ? 12 : 4} lg={_this.props.smallModal ? 12 : 4}>
                          <div className="actions-right">
                            <a className="editLink" target="_blank"
                              onClick={() => {
                                downloadFile(
                                  ("common/" + _this.props.documents.substring(0, _this.props.documents.lastIndexOf("/") + 1)),
                                  _this.props.documents.substring(_this.props.documents.lastIndexOf("/") + 1, _this.props.documents.length),
                                  null,
                                  (res) => {
                                    const file = new Blob([res.data], { type: res.data.type });
                                    const fileURL = URL.createObjectURL(file);
                                    // Open the URL on new Window
                                    window.open(fileURL, '_blank');
                                  },
                                  (error) => {
                                    console.log(error)
                                    alert(error)
                                  }
                                )
                              }}
                            >
                              {((this.props.documents.split('/')).pop()).substring(this.props.documents.split('/').pop().lastIndexOf("_") + 1)}
                            </a>
                            <a className="fa fa-trash text-danger" onClick={() => this.deleteConfirmMessage(this.props.documents, 0)}>{null}</a>
                          </div>
                        </Col>
                      ) : (
                        this.props.documents && this.props.documents.length
                          ? (
                            this.props.documents.map((document, key) => {
                              let _this = this;
                              return (
                                <Col xs={12} sm={this.props.smallModal ? 12 : 6} md={this.props.smallModal ? 12 : 4} lg={this.props.smallModal ? 12 : 4} key={key}>
                                  <div className="actions-right">
                                    <a className="editLink" target="_blank"
                                      // href={backendURL + document}>
                                      onClick={() => {
                                        downloadFile(
                                          ("common/" + document.substring(0, document.lastIndexOf("/") + 1)),
                                          document.substring(document.lastIndexOf("/") + 1, document.length),
                                          null,
                                          (res) => {
                                            const file = new Blob([res.data], { type: res.data.type });
                                            const fileURL = URL.createObjectURL(file);
                                            // Open the URL on new Window
                                            window.open(fileURL, '_blank');
                                          },
                                          (error) => {
                                            console.log(error)
                                            alert(error)
                                          }
                                        )
                                      }}
                                    >
                                      {((document.split('/')).pop()).substring(document.split('/').pop().lastIndexOf("_") + 1)}
                                    </a>
                                    <a className="fa fa-trash text-danger" onClick={() => _this.deleteConfirmMessage(document, key)}>{null}</a>
                                  </div>
                                </Col>
                              )
                            })
                          ) : null
                      )
                  }
                  {
                    this.props.drawings
                      ? (
                        <Col xs={12} style={{ marginTop: "10px" }}>
                          <div className="">
                            {/* <Button className={(this.state.documentUplader) ? "hideDiv" : "uploadButton"} ></Button> */}
                            <input className={(this.state.documentUplader) ? "hideDiv" : null} type="file" ref="fileInput" onChange={(e) => this.handleDocumentChange(e)} multiple />
                          </div>
                        </Col>
                      ) : (
                        <Col xs={12} style={{ marginTop: "10px" }}>
                          <div className="upload-btn-wrapper">
                            <Button className={(this.state.documentUplader) ? "hideDiv" : "uploadButton"} ><i className="fa fa-upload"></i>{this.props.dropText}</Button>
                            <input className={(this.state.documentUplader) ? "hideDiv" : null} type="file" ref="fileInput" onChange={(e) => this.handleDocumentChange(e)} multiple />
                          </div>
                        </Col>
                      )
                  }


                  {
                    !this.props.drawings
                      ? (
                        <Col md={12} className={(this.state.documentUplader) ? null : "hideDiv"} style={{ marginTop: "10px" }}>
                          <div className="progress-line-primary progress">
                            <div role="progressbar" className="progress-bar" aria-valuenow={this.state.completed} aria-valuemin="0" aria-valuemax="100" style={{ width: this.state.completed + "%" }}>
                              <span className="sr-only"></span>
                            </div>
                          </div>
                        </Col>
                      ) : null
                  }
                </div>
              ) : ""
          }
        </div>
      </div>
    );
  }
}

export default UploadComponent;
