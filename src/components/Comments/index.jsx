import React, { Component } from "react";
import { Col, FormGroup, FormControl, ControlLabel, OverlayTrigger, Tooltip } from "react-bootstrap";
import Moment from "moment";
import _ from "lodash";
import cookie from "react-cookies";

import Button from "components/CustomButton/CustomButton.jsx";
import Datetime from "react-datetime";

export default class CommentsComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // showCommentsModal: false,
      commentObject: props.obj,
      formError: null,
      notesValid: null,
    }
  }

  // componentWillReceiveProps(newProps) {
  //   if (newProps.showCommentsModal !== this.state.showCommentsModal) {
  //     this.setState({
  //       showCommentsModal: newProps.showCommentsModal,
  //       commentObject: newProps.obj
  //     })
  //   }
  // }

  render() {
    return (
      <div>
        {
          this.state.commentObject !== null
            ? (
              <div>
                <Col xs={12} sm={6}>
                  <ControlLabel>Last follow up date</ControlLabel>
                  <Datetime
                    disabled={true}
                    timeFormat={false}
                    closeOnSelect={true}
                    dateFormat="DD/MM/YYYY"
                    name="lastFollowup"
                    inputProps={{ placeholder: "Select start date" }}
                    value={
                      this.state.commentObject && this.state.commentObject.lastFollowup
                        ? Moment(this.state.commentObject.lastFollowup).format("DD/MM/YYYY")
                        : "--"
                    }
                    onChange={() => { }}
                  />
                </Col>
                <Col xs={12} sm={6}>
                  <ControlLabel>{this.props.quotation ? "Due Date" : "Next follow up date"}</ControlLabel>
                  <Datetime
                    timeFormat={false}
                    closeOnSelect={true}
                    dateFormat="DD/MM/YYYY"
                    name="nextFollowup"
                    inputProps={{ placeholder: "Select next followup date" }}
                    value={
                      this.state.commentObject && this.state.commentObject.nextFollowup
                        ? Moment(this.state.commentObject.nextFollowup).format("DD/MM/YYYY")
                        : "--"
                    }
                    onChange={(date) => {
                      let temp = this.state.commentObject;
                      temp.nextFollowup = date._d;
                      this.setState({ temp });
                    }}
                  />
                </Col>
                <Col xs={12}>
                  <FormGroup>
                    <div>
                      {
                        this.state.commentObject !== null
                          ? this.state.commentObject && this.state.commentObject.comments.length
                            ? this.state.commentObject && this.state.commentObject.comments.map((x, key) => {
                              // let comments = (
                              //   <div>
                              //     {x.notes ? x.notes : ""} <br />
                              //     {x.user ? x.user.name + " @ " : " @ "}  {x.createdAt ? Moment(x.createdAt).format('DD/MM/YYYY hh:mm A') : ""}
                              //   </div>
                              // )
                              return (
                                <div>
                                  <hr />
                                  {x.notes ? x.notes : ""} <br />
                                  <small>
                                    {x.user ? x.user.name + " @ " : " @ "}  {x.createdAt ? Moment(x.createdAt).format('DD/MM/YYYY hh:mm A') : ""}
                                  </small>
                                </div>
                              )
                            })
                            : null
                          : null
                      }
                    </div>
                  </FormGroup>
                  <hr />
                  <FormGroup>
                    <ControlLabel>Add comment<span className="star">*</span>  </ControlLabel>
                    <FormControl
                      componentClass="textarea"
                      rows={5}
                      placeholder="Enter comment..."
                      type="text"
                      name="notes"
                      value={this.state.commentObject ? this.state.commentObject.notes : ""}
                      onChange={(e) => {
                        let tempObj = this.state.commentObject;
                        tempObj.notes = e.target.value;
                        this.setState({ tempObj })
                      }}
                      className={this.state.notesValid || this.state.notesValid === null ? "" : "error"}

                    />
                  </FormGroup>
                </Col>
                <Col md={12} lg={12} sm={12} xs={12}>
                  <OverlayTrigger placement="top" overlay={<Tooltip id="save_tooltip">Save Comment</Tooltip>}>
                    <Button bsStyle="success" fill icon pullRight onClick={() => {
                      let temp = this.state.commentObject;
                      let _this = this;

                      if (temp.notes && temp.notes !== "") {
                        this.setState({ notesValid: true })
                        temp.comments && temp.comments.unshift({
                          createdAt: Date.now(),
                          notes: temp.notes,
                          user: {
                            name: cookie.load('user'),
                            id: cookie.load('id')
                          }
                        })


                        this.setState({ temp, showCommentsModal: false });
                        let update = JSON.parse(JSON.stringify(temp));
                        update.lastFollowup = Date.now();

                        update.comments.map(comment => {
                          comment.user = comment.user && (comment.user.id || comment.user._id)
                            ? comment.user.id || comment.user._id
                            : null;
                          return true;
                        })

                        _this.props.hideCommentsModal();
                        _this.props.updateObj(
                          _.pick(update, ["code", "comments", "lastFollowup", "nextFollowup", "dueDate"]),
                          () => _this.props.successAlert("Comments saved successfully!"),
                          () => _this.props.errorAlert("Something went wrong!")
                        )
                      }
                      else {
                        this.setState({ notesValid: false });
                        this.setState({ formError: "Please enter required fields" });
                      }
                    }}
                    ><span className="fa fa-save"></span></Button>
                  </OverlayTrigger>
                </Col>
                <center><b><h5 className="text-danger">{this.state.formError}</h5></b></center>
              </div>
            ) : null
        }
      </div>
    )
  }
};
