import React, { Component } from "react";
import { Col, FormGroup, FormControl, ControlLabel, } from "react-bootstrap";
import moment from "moment"
import Datetime from "react-datetime";

class CustomFilterComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filter: props.filter ? props.filter : {},
    }
  }

  filter = (e, name) => {
    let _this = this;
    let tempFilter = _this.props.filter;
    if (name === "startDate" || name === "endDate") {
      tempFilter[name] = e._d;
    } else {
      tempFilter[name] = e ? e.target.value : "";
    }
    _this.setState({ tempFilter, page: 0 });
    setTimeout(() => this.props.fetchDataFromServer(tempFilter), 100);
  }
  
  render() {
    return (
      <div className="list-header">
        {
          this.props.startDate
            ? (
              <Col xs={12} sm={6} md={2} lg={2}>
                <FormGroup>
                  <ControlLabel>Start Date</ControlLabel>
                  <Datetime
                    // className="form-control"
                    timeFormat={false}
                    closeOnSelect={true}
                    dateFormat="DD/MMM/YYYY"
                    name="startDate"
                    inputProps={{ placeholder: "Select start date" }}
                    value={moment(this.props.filter.startDate).format("DD/MMM/YYYY")}
                    onChange={(date) => this.filter(date, "startDate")}
                    isValidDate={(e) => e.endOf('day').valueOf() <= moment().endOf('day').valueOf()}
                  />
                </FormGroup>
              </Col>
            )
            : null
        }
        {
          this.props.endDate
            ? (
              <Col xs={12} sm={6} md={2} lg={2}>
                <FormGroup>
                  <ControlLabel>End Date</ControlLabel>
                  <Datetime
                    timeFormat={false}
                    closeOnSelect={true}
                    dateFormat="DD/MMM/YYYY"
                    name="endDate"
                    inputProps={{ placeholder: "Select end date" }}
                    value={moment(this.props.filter.endDate).format("DD/MMM/YYYY")}
                    onChange={(date) => this.filter(date, "endDate")}
                    isValidDate={(e) => e.endOf('day').valueOf() <= moment().endOf('day').valueOf()}
                  />
                </FormGroup>
              </Col>
            )
            : null
        }
        {
          this.props.name
            ? (
              <Col xs={12} sm={6} md={3} lg={3}>
                <FormGroup>
                  <FormControl
                    type="text"
                    name="name"
                    placeholder="Search by name"
                    onChange={(e) => this.filter(e, "name")}
                  />
                </FormGroup>
              </Col>
            ) : null
        }
        {
          this.props.number
            ? (
              <Col xs={12} sm={6} md={3} lg={3}>
                <FormGroup>
                  <FormControl
                    type="text"
                    name="name"
                    placeholder="Search by number"
                    onChange={(e) => this.filter(e, "number")}
                  />
                </FormGroup>
              </Col>
            ) : null
        }
        {
          this.props.email
            ? (
              <Col xs={12} sm={6} md={3} lg={3}>
                <FormGroup>
                  <FormControl
                    type="text"
                    name="email"
                    placeholder="Search by email"
                    onChange={(e) => this.filter(e, "email")}
                  />
                </FormGroup>
              </Col>
            ) : null
        }
        {
          this.props.city
            ? (
              <Col xs={12} sm={6} md={3} lg={3}>
                <FormGroup>
                  <FormControl
                    type="text"
                    name="city"
                    placeholder="Search by city"
                    onChange={(e) => this.filter(e, "city")}
                  />
                </FormGroup>
              </Col>
            ) : null
        }
        {
          this.props.category
            ? (
              <Col xs={12} sm={6} md={3} lg={3}>
                <FormGroup>
                  <FormControl
                    type="text"
                    name="category"
                    placeholder="Search by category"
                    onChange={(e) => this.filter(e, "category")}
                  />
                </FormGroup>
              </Col>
            )
            : null
        }
        {
          this.props.type
            ? (
              <Col xs={12} sm={6} md={3} lg={3}>
                <FormGroup>
                  <FormControl
                    type="text"
                    name="type"
                    placeholder="Search by type"
                    onChange={(e) => this.filter(e, "type")}
                  />
                </FormGroup>
              </Col>
            )
            : null
        }
        {this.props.hsn
          ?
          (
            <Col xs={12} sm={6} md={3} lg={3}>
              <FormGroup>
                <FormControl
                  type="text"
                  name="hsn"
                  placeholder="Search by HSN code"
                  onChange={(e) => this.filter(e, "hsn")}
                />
              </FormGroup>
            </Col>
          )
          : null
        }
        {
          this.props.customerName
            ? (
              <Col xs={12} sm={6} md={3} lg={3}>
                <FormGroup>
                  <FormControl
                    type="text"
                    name="customerName"
                    placeholder="Search by customer name"
                    onChange={(e) => this.filter(e, "customerName")}
                  />
                </FormGroup>
              </Col>
            )
            : null
        }
        {
          this.props.user
            ? (
              <Col xs={12} sm={6} md={3} lg={3}>
                <FormGroup>
                  <FormControl
                    type="text"
                    name="user"
                    placeholder="Search by  salesRepresentative"
                    onChange={(e) => this.filter(e, "user")}
                  />
                </FormGroup>
              </Col>
            )
            : null
        }
         {
          this.props.searchInvoice
            ? (
              <Col xs={12} sm={6} md={3} lg={3}>
                <FormGroup>
                <ControlLabel>Invoice Number</ControlLabel>
                  <FormControl
                    type="text"
                    name="searchInvoice"
                    placeholder="Search by invoice number"
                    onChange={(e) => this.filter(e, "searchInvoice")}
                  />
                </FormGroup>
              </Col>
            )
            : null
        }
      </div>
    )
  }
}

export default CustomFilterComponent;
