import React, { Component } from "react";
import HomePageComponent from "../components/HomePageComponent.jsx";

class AddEditHomePage extends Component {
  render() {
    return (
      <HomePageComponent settings={false} {...this.props}></HomePageComponent>
    )
  }
}

export default AddEditHomePage;