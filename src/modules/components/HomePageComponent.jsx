import React, { Component } from "react";

import jackSlide from  "assets/images/jack-slide.png";
import jackpot from  "assets/images/jackpot.png";
import csBoo from  "assets/images/cs-boo.png";
import liberty from  "assets/images/liberty.png";
import euro from  "assets/images/euro.png";
import euroslot from  "assets/images/euroslot.png";
import addBanner from  "assets/images/add-banner.png";
import betThree from  "assets/images/bet-three.png";
import evobet from  "assets/images/evobet.png";
import evo from  "assets/images/evo.png";
import social1 from  "assets/images/social1.png";
import social2 from  "assets/images/social2.png";
import social3 from  "assets/images/social3.png";
import joinLogo from  "assets/images/join-logo.png";
import join from  "assets/images/join.png";
import forum2 from  "assets/images/forum2.png";
import forum from  "assets/images/forum.png";
import pen from  "assets/images/pen.png";
import latest from  "assets/images/latest.png";
import msg from  "assets/images/msg.png";
import ns6 from  "assets/images/ns6.png";
import ns5 from  "assets/images/ns5.png";
import ns4 from  "assets/images/ns4.png";
import womania from  "assets/images/womania.png";
import ns3 from  "assets/images/ns3.png";
import ns2 from  "assets/images/ns2.png";
import ns1 from  "assets/images/ns1.png";
import aussie from  "assets/images/aussie.png";
import { FormGroup, ControlLabel, FormControl, Row, Col, Tooltip, OverlayTrigger } from "react-bootstrap";
import HeaderPage from "../../views/HeaderPage.jsx"
import FooterPage from "../../views/FooterPage.jsx"
import $ from 'jquery';
import ScriptTag from 'react-script-tag';

class HomePageComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      settings: props.settings,    
      home: {
       
      },     
    }
   
  }
  componentWillMount() 
  {
 
   
}
 
  render() {
      let form = (
      <div>
      <HeaderPage {...this.props}></HeaderPage>
      <div class="site-content">     
     <section class="section-feature">
         <div class="container">
            <div class="row">
               <div class="col-sm-12">
                 <span class="flag-e sk">OCR <strong>Best Casinos in UK</strong></span>
               </div>
             </div>
             <div class="row">
               <div class="col-sm-7 wow fadeInLeft">
                     <div class="jackpot-slider">
                      <div id="jackpot" class="owl-carousel">
                         
                         <div>
                           <div class="euro-div">
                             <img src={jackSlide} />
                             <div class="euro-text">
                             Euro Slot Casino Home Page</div>
                           </div>
                         </div>
                         <div>
                           <div class="euro-div">
                             <img src={jackSlide} />
                             <div class="euro-text">
                             Euro Slot Casino Home Page</div>
                           </div>
                         </div>
                         <div>
                           <div class="euro-div">
                             <img src={jackSlide} />
                             <div class="euro-text">
                             Euro Slot Casino Home Page</div>
                           </div>
                         </div>
                         <div>
                           <div class="euro-div">
                             <img src={jackSlide} />
                             <div class="euro-text">
                             Euro Slot Casino Home Page</div>
                           </div>
                         </div>
                       

                     

                      </div>
                 </div>
                 </div>
               <div class="col-sm-5 wow fadeInRight">
                 <div class="jackpot-img">
                   <img src={jackpot}/>
                 </div>
                 <p>
                     Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing.
                   </p>

                   <ul class="btn-euro">
                     <li><a href="" class="jack-bt">Jackpot City Full Review</a></li>
                     <li><a href="" class="play-now">Play-Now</a></li>
                   </ul>

               </div>
             </div>
           </div>


      </section>

      <section class="section-top">
        <div class="container">
          <div class="row">
            <div class="col-sm-8">
               <span class="flag-e sk wh">OCR <strong>Top UK Casinos</strong></span>
               <p>
                 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
               </p>
            </div>
             <div class="col-sm-4">
               <a href="#">View all Top UK Casinos</a>
            </div>
          </div>
        </div>
      </section>

      <section class="section-casino-list">
        <div class="container">
           <div class="casinos-list">
               <div class="row">
                 <div class="col-sm-1 img-cs">
                    <div class="img-cas">
                     <ul class="rate">
                       <li><i class="fas fa-star"></i></li>
                       <li><i class="fas fa-star"></i></li>
                       <li><i class="fas fa-star"></i></li>
                       <li><i class="fas fa-star"></i></li>
                     </ul>
                     <img src={csBoo}/>
                   </div>
                 </div>
                 <div class="col-sm-3 div-cas">
                   <div class="boo-casino">
                     <h6>Boo Casino</h6>
                     <p>
                       Casino with flashy and attractive design reminiscent of bright night Vegas. Apart from a large variety video slots and online table.
                     </p>
                   </div>
                 </div>
                 <div class="col-sm-4 st-bonus">
                     <div class="div-bonus">
                      <div class="row">
                        <div class="col-sm-6 null-p">
                          <h6>Exclusive Bonus</h6>
                          <p>$66 No Deposit Bonus</p>
                        </div>
                        <div class="col-sm-6 null-p">
                           <a href="#">Get Bonus</a>
                        </div>
                        
                      </div>
                    </div>
                 </div>
                 <div class="col-sm-2">
                   <div class="deposit-text">
                       <h6>Minimum Deposit</h6>
                       <p>$10</p>
                       <h6>Max Cashout</h6>
                       <p>No</p>
                   </div>
                 </div>
                 <div class="col-sm-4">
                     <ul class="btn-bbo">
                       <li>
                         <a href="#" class="bbo-bt">BBO Casino Full Review</a>
                       </li>
                       <li>
                         <a href="#" class="play-now">Play-Now</a>
                       </li>
                     </ul>
                 </div>
               </div>
           
           </div>
            <div class="casinos-list">
               <div class="row">
                 <div class="col-sm-1 img-cs">
                    <div class="img-cas">
                     <ul class="rate">
                       <li><i class="fas fa-star"></i></li>
                       <li><i class="fas fa-star"></i></li>
                       <li><i class="fas fa-star"></i></li>
                       <li><i class="fas fa-star"></i></li>
                     </ul>
                     <img src={csBoo}/>
                   </div>
                 </div>
                 <div class="col-sm-3 div-cas">
                   <div class="boo-casino">
                     <h6>Boo Casino</h6>
                     <p>
                       Casino with flashy and attractive design reminiscent of bright night Vegas. Apart from a large variety video slots and online table.
                     </p>
                   </div>
                 </div>
                 <div class="col-sm-4 st-bonus">
                     <div class="div-bonus">
                      <div class="row">
                        <div class="col-sm-6 null-p">
                          <h6>Exclusive Bonus</h6>
                          <p>$66 No Deposit Bonus</p>
                        </div>
                        <div class="col-sm-6 null-p">
                           <a href="#">Get Bonus</a>
                        </div>
                        
                      </div>
                    </div>
                 </div>
                 <div class="col-sm-2">
                   <div class="deposit-text">
                       <h6>Minimum Deposit</h6>
                       <p>$10</p>
                       <h6>Max Cashout</h6>
                       <p>No</p>
                   </div>
                 </div>
                 <div class="col-sm-4">
                     <ul class="btn-bbo">
                       <li>
                         <a href="#" class="bbo-bt">BBO Casino Full Review</a>
                       </li>
                       <li>
                         <a href="#" class="play-now">Play-Now</a>
                       </li>
                     </ul>
                 </div>
               </div>
           
           </div>
           <div class="casinos-list">
               <div class="row">
                 <div class="col-sm-1 img-cs">
                    <div class="img-cas">
                     <ul class="rate">
                       <li><i class="fas fa-star"></i></li>
                       <li><i class="fas fa-star"></i></li>
                       <li><i class="fas fa-star"></i></li>
                       <li><i class="fas fa-star"></i></li>
                     </ul>
                     <img src={csBoo}/>
                   </div>
                 </div>
                 <div class="col-sm-3 div-cas">
                   <div class="boo-casino">
                     <h6>Boo Casino</h6>
                     <p>
                       Casino with flashy and attractive design reminiscent of bright night Vegas. Apart from a large variety video slots and online table.
                     </p>
                   </div>
                 </div>
                 <div class="col-sm-4 st-bonus">
                     <div class="div-bonus">
                      <div class="row">
                        <div class="col-sm-6 null-p">
                          <h6>Exclusive Bonus</h6>
                          <p>$66 No Deposit Bonus</p>
                        </div>
                        <div class="col-sm-6 null-p">
                           <a href="#">Get Bonus</a>
                        </div>
                        
                      </div>
                    </div>
                 </div>
                 <div class="col-sm-2">
                   <div class="deposit-text">
                       <h6>Minimum Deposit</h6>
                       <p>$10</p>
                       <h6>Max Cashout</h6>
                       <p>No</p>
                   </div>
                 </div>
                 <div class="col-sm-4">
                     <ul class="btn-bbo">
                       <li>
                         <a href="#" class="bbo-bt">BBO Casino Full Review</a>
                       </li>
                       <li>
                         <a href="#" class="play-now">Play-Now</a>
                       </li>
                     </ul>
                 </div>
               </div>
           
           </div>
            <div class="casinos-list">
               <div class="row">
                 <div class="col-sm-1 img-cs">
                    <div class="img-cas">
                     <ul class="rate">
                       <li><i class="fas fa-star"></i></li>
                       <li><i class="fas fa-star"></i></li>
                       <li><i class="fas fa-star"></i></li>
                       <li><i class="fas fa-star"></i></li>
                     </ul>
                     <img src={csBoo}/>
                   </div>
                 </div>
                 <div class="col-sm-3 div-cas">
                   <div class="boo-casino">
                     <h6>Boo Casino</h6>
                     <p>
                       Casino with flashy and attractive design reminiscent of bright night Vegas. Apart from a large variety video slots and online table.
                     </p>
                   </div>
                 </div>
                 <div class="col-sm-4 st-bonus">
                     <div class="div-bonus">
                      <div class="row">
                        <div class="col-sm-6 null-p">
                          <h6>Exclusive Bonus</h6>
                          <p>$66 No Deposit Bonus</p>
                        </div>
                        <div class="col-sm-6 null-p">
                           <a href="#">Get Bonus</a>
                        </div>
                        
                      </div>
                    </div>
                 </div>
                 <div class="col-sm-2">
                   <div class="deposit-text">
                       <h6>Minimum Deposit</h6>
                       <p>$10</p>
                       <h6>Max Cashout</h6>
                       <p>No</p>
                   </div>
                 </div>
                 <div class="col-sm-4">
                     <ul class="btn-bbo">
                       <li>
                         <a href="#" class="bbo-bt">BBO Casino Full Review</a>
                       </li>
                       <li>
                         <a href="#" class="play-now">Play-Now</a>
                       </li>
                     </ul>
                 </div>
               </div>
           
           </div>
            <div class="casinos-list">
               <div class="row">
                 <div class="col-sm-1 img-cs">
                    <div class="img-cas">
                     <ul class="rate">
                       <li><i class="fas fa-star"></i></li>
                       <li><i class="fas fa-star"></i></li>
                       <li><i class="fas fa-star"></i></li>
                       <li><i class="fas fa-star"></i></li>
                     </ul>
                     <img src={csBoo}/>
                   </div>
                 </div>
                 <div class="col-sm-3 div-cas">
                   <div class="boo-casino">
                     <h6>Boo Casino</h6>
                     <p>
                       Casino with flashy and attractive design reminiscent of bright night Vegas. Apart from a large variety video slots and online table.
                     </p>
                   </div>
                 </div>
                 <div class="col-sm-4 st-bonus">
                     <div class="div-bonus">
                      <div class="row">
                        <div class="col-sm-6 null-p">
                          <h6>Exclusive Bonus</h6>
                          <p>$66 No Deposit Bonus</p>
                        </div>
                        <div class="col-sm-6 null-p">
                           <a href="#">Get Bonus</a>
                        </div>
                        
                      </div>
                    </div>
                 </div>
                 <div class="col-sm-2">
                   <div class="deposit-text">
                       <h6>Minimum Deposit</h6>
                       <p>$10</p>
                       <h6>Max Cashout</h6>
                       <p>No</p>
                   </div>
                 </div>
                 <div class="col-sm-4">
                     <ul class="btn-bbo">
                       <li>
                         <a href="#" class="bbo-bt">BBO Casino Full Review</a>
                       </li>
                       <li>
                         <a href="#" class="play-now">Play-Now</a>
                       </li>
                     </ul>
                 </div>
               </div>
           
           </div>
            <div class="casinos-list">
               <div class="row">
                 <div class="col-sm-1 img-cs">
                    <div class="img-cas">
                     <ul class="rate">
                       <li><i class="fas fa-star"></i></li>
                       <li><i class="fas fa-star"></i></li>
                       <li><i class="fas fa-star"></i></li>
                       <li><i class="fas fa-star"></i></li>
                     </ul>
                     <img src={csBoo}/>
                   </div>
                 </div>
                 <div class="col-sm-3 div-cas">
                   <div class="boo-casino">
                     <h6>Boo Casino</h6>
                     <p>
                       Casino with flashy and attractive design reminiscent of bright night Vegas. Apart from a large variety video slots and online table.
                     </p>
                   </div>
                 </div>
                 <div class="col-sm-4 st-bonus">
                     <div class="div-bonus">
                      <div class="row">
                        <div class="col-sm-6 null-p">
                          <h6>Exclusive Bonus</h6>
                          <p>$66 No Deposit Bonus</p>
                        </div>
                        <div class="col-sm-6 null-p">
                           <a href="#">Get Bonus</a>
                        </div>
                        
                      </div>
                    </div>
                 </div>
                 <div class="col-sm-2">
                   <div class="deposit-text">
                       <h6>Minimum Deposit</h6>
                       <p>$10</p>
                       <h6>Max Cashout</h6>
                       <p>No</p>
                   </div>
                 </div>
                 <div class="col-sm-4">
                     <ul class="btn-bbo">
                       <li>
                         <a href="#" class="bbo-bt">BBO Casino Full Review</a>
                       </li>
                       <li>
                         <a href="#" class="play-now">Play-Now</a>
                       </li>
                     </ul>
                 </div>
               </div>
           
           </div>
            <div class="casinos-list">
               <div class="row">
                 <div class="col-sm-1 img-cs">
                    <div class="img-cas">
                     <ul class="rate">
                       <li><i class="fas fa-star"></i></li>
                       <li><i class="fas fa-star"></i></li>
                       <li><i class="fas fa-star"></i></li>
                       <li><i class="fas fa-star"></i></li>
                     </ul>
                     <img src={csBoo}/>
                   </div>
                 </div>
                 <div class="col-sm-3 div-cas">
                   <div class="boo-casino">
                     <h6>Boo Casino</h6>
                     <p>
                       Casino with flashy and attractive design reminiscent of bright night Vegas. Apart from a large variety video slots and online table.
                     </p>
                   </div>
                 </div>
                 <div class="col-sm-4 st-bonus">
                     <div class="div-bonus">
                      <div class="row">
                        <div class="col-sm-6 null-p">
                          <h6>Exclusive Bonus</h6>
                          <p>$66 No Deposit Bonus</p>
                        </div>
                        <div class="col-sm-6 null-p">
                           <a href="#">Get Bonus</a>
                        </div>
                        
                      </div>
                    </div>
                 </div>
                 <div class="col-sm-2">
                   <div class="deposit-text">
                       <h6>Minimum Deposit</h6>
                       <p>$10</p>
                       <h6>Max Cashout</h6>
                       <p>No</p>
                   </div>
                 </div>
                 <div class="col-sm-4">
                     <ul class="btn-bbo">
                       <li>
                         <a href="#" class="bbo-bt">BBO Casino Full Review</a>
                       </li>
                       <li>
                         <a href="#" class="play-now">Play-Now</a>
                       </li>
                     </ul>
                 </div>
               </div>
           
           </div>
            <div class="casinos-list">
               <div class="row">
                 <div class="col-sm-1 img-cs">
                    <div class="img-cas">
                     <ul class="rate">
                       <li><i class="fas fa-star"></i></li>
                       <li><i class="fas fa-star"></i></li>
                       <li><i class="fas fa-star"></i></li>
                       <li><i class="fas fa-star"></i></li>
                     </ul>
                     <img src={csBoo}/>
                   </div>
                 </div>
                 <div class="col-sm-3 div-cas">
                   <div class="boo-casino">
                     <h6>Boo Casino</h6>
                     <p>
                       Casino with flashy and attractive design reminiscent of bright night Vegas. Apart from a large variety video slots and online table.
                     </p>
                   </div>
                 </div>
                 <div class="col-sm-4 st-bonus">
                     <div class="div-bonus">
                      <div class="row">
                        <div class="col-sm-6 null-p">
                          <h6>Exclusive Bonus</h6>
                          <p>$66 No Deposit Bonus</p>
                        </div>
                        <div class="col-sm-6 null-p">
                           <a href="#">Get Bonus</a>
                        </div>
                        
                      </div>
                    </div>
                 </div>
                 <div class="col-sm-2">
                   <div class="deposit-text">
                       <h6>Minimum Deposit</h6>
                       <p>$10</p>
                       <h6>Max Cashout</h6>
                       <p>No</p>
                   </div>
                 </div>
                 <div class="col-sm-4">
                     <ul class="btn-bbo">
                       <li>
                         <a href="#" class="bbo-bt">BBO Casino Full Review</a>
                       </li>
                       <li>
                         <a href="#" class="play-now">Play-Now</a>
                       </li>
                     </ul>
                 </div>
               </div>
           
           </div>


        </div>
      </section>

      <section class="section-newest">
         <div class="container">
           <div class="row">
             <div class="col-sm-12">
                <span class="flag-e sk">OCR <strong>Newest UK Casino</strong></span>
             </div>
           </div>
           <div class="row">
             <div class="col-sm-3">
               <div class="img-newest">
                 <img src={liberty}/>
               </div>
             </div>
             <div class="col-sm-5">
               <div class="text-newest">
                 <h5>Liberty Slot Casino</h5>
                 <p>
                   Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                 </p>
               </div>
             </div>
             <div class="col-sm-4">
                 <ul class="btn-bbo">
                   <li>
                     <a href="#" class="bbo-bt">Liberty Slots Full Review</a>
                   </li>
                   <li>
                     <a href="#" class="play-now">Play-Now</a>
                   </li>
                 </ul>
             </div>
           </div>
         </div>
      </section>

      <section class="section-top">
        <div class="container">
          <div class="row">
            <div class="col-sm-8">
               <span class="flag-e sk wh">OCR <strong>Newest UK Slot Games</strong></span>
               <p>
                 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
               </p>
            </div>
             <div class="col-sm-4">
               <a href="#">View All UK Slot Games</a>
            </div>
          </div>
        </div>
      </section>
      <section class="section-feature euroslots">
         <div class="container">
            <div class="row">
               <div class="col-sm-12">
                 <span class="flag-e sk">OCR <strong>Best Casinos in UK</strong></span>
               </div>
             </div>
             <div class="row">
               <div class="col-sm-7">
                  <div class="jackpot-slider">
                      <div id="euroslots" class="owl-carousel">
                         
                         <div>
                           <div class="euro-div">
                             <img src={euro} />
                             <div class="euro-text">
                             Euro Slot Casino Home Page</div>
                           </div>
                         </div>
                         <div>
                           <div class="euro-div">
                             <img src={euro} />
                             <div class="euro-text">
                             Euro Slots Casino Home Page</div>
                           </div>
                         </div>
                         <div>
                           <div class="euro-div">
                             <img src={euro} />
                             <div class="euro-text">
                             Euro Slots Casino Home Page</div>
                           </div>
                         </div>
                         <div>
                           <div class="euro-div">
                             <img src={euro} />
                             <div class="euro-text">
                             Euro Slots Casino Home Page</div>
                           </div>
                         </div>

                      </div>
                 </div>
               </div>
               <div class="col-sm-5">
                 <div class="jackpot-img">
                   <img src={euroslot}/>
                 </div>
                 <p>
                     Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing.
                   </p>

                   <ul class="btn-euro">
                     <li><a href="" class="jack-bt">Euro Slots Full Review</a></li>
                     <li><a href="" class="play-now">Play-Now</a></li>
                   </ul>

               </div>
             </div>
           </div>


      </section>
      <section class="section-advertise">
         <div class="row">
           <div class="col-sm-12 text-center">
             <a href="#">
               <div class="ad-image">
                 <img src={addBanner}/>
               </div>
             </a>
           </div>
         </div>
      </section>
       <section class="section-top">
        <div class="container">
          <div class="row">
            <div class="col-sm-8">
               <span class="flag-e sk wh">OCR <strong>Featured UK Live Casino</strong></span>
               <p>
                 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
               </p>
            </div>
             <div class="col-sm-4">
               <a href="#">View All UK Live Casinos</a>
            </div>
          </div>
        </div>
      </section>

      <section class="section-feature euroslots bet">
         <div class="container">
            <div class="row">
               <div class="col-sm-12">
                 <span class="flag-e sk">OCR <strong>Best Casinos in UK</strong></span>
               </div>
             </div>
             <div class="row">
               <div class="col-sm-7">
                  <div class="jackpot-slider">
                      <div id="bet365" class="owl-carousel">
                         
                         <div>
                           <div class="euro-div">
                             <img src="images/bet.png" />
                             <div class="euro-text">
                             Bet365 Live Casino Roulette</div>
                           </div>
                         </div>
                         <div>
                           <div class="euro-div">
                             <img src="images/bet.png" />
                             <div class="euro-text">
                             Bet365 Live Casino Roulette</div>
                           </div>
                         </div>
                         <div>
                           <div class="euro-div">
                             <img src="images/bet.png" />
                             <div class="euro-text">
                             Bet365 Live Casino Roulette</div>
                           </div>
                         </div>
                         <div>
                           <div class="euro-div">
                             <img src="images/bet.png" />
                             <div class="euro-text">
                             Bet365 Live Casino Roulette</div>
                           </div>
                         </div>

                      </div>
                 </div>
               </div>
               <div class="col-sm-5">
                 <div class="jackpot-img">
                   <img src={betThree}/>
                 </div>
                 <p>
                     Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing.
                   </p>

                   <ul class="btn-euro">
                     <li><a href="" class="jack-bt">Bet 365 Full Review</a></li>
                     <li><a href="" class="play-now">Play-Now</a></li>
                   </ul>

               </div>
             </div>
           </div>


      </section>
       <section class="section-top">
          <div class="container">
            <div class="row">
              <div class="col-sm-8">
                 <span class="flag-e sk wh">OCR <strong>Featured UK Sports Betting</strong></span>
                 <p>
                   Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                 </p>
              </div>
               <div class="col-sm-4">
                 <a href="#">View all UK Sports Betting</a>
              </div>
            </div>
          </div>
       </section>

       <section class="section-feature euroslots ev">
         <div class="container">
            <div class="row">
               <div class="col-sm-12">
                 <span class="flag-e sk">OCR <strong>Best Casinos in UK</strong></span>
               </div>
             </div>
             <div class="row">
               <div class="col-sm-7">
                  <div class="jackpot-slider">
                      <div id="evobet" class="owl-carousel">
                         
                         <div>
                           <div class="euro-div">
                             <img src={evobet} />
                             <div class="euro-text">
                             Evo Bet Home Page</div>
                           </div>
                         </div>
                         <div>
                           <div class="euro-div">
                             <img src={evobet} />
                             <div class="euro-text">
                             Evo Bet Home Page</div>
                           </div>
                         </div>
                         <div>
                           <div class="euro-div">
                             <img src={evobet} />
                             <div class="euro-text">
                             Evo Bet Home Page</div>
                           </div>
                         </div>
                         <div>
                           <div class="euro-div">
                             <img src={evobet} />
                             <div class="euro-text">
                             Evo Bet Home Page</div>
                           </div>
                         </div>

                      </div>
                 </div>
               </div>
               <div class="col-sm-5">
                 <div class="jackpot-img">
                   <img src={evo}/>
                 </div>
                 <p>
                     Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing.
                   </p>

                   <ul class="btn-euro">
                     <li><a href="" class="jack-bt">Evo Bet Full Review</a></li>
                     <li><a href="" class="play-now">Play-Now</a></li>
                   </ul>

               </div>
             </div>
           </div>


       </section>
       <section class="section-top">
          <div class="container">
            <div class="row">
              <div class="col-sm-8">
                 <span class="flag-e sk wh">OCR <strong>Featured UK Poker</strong></span>
                 <p>
                   Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                 </p>
              </div>
               <div class="col-sm-4">
                 <a href="#">View all UK Sports Betting</a>
              </div>
            </div>
          </div>
       </section>

       <section class="section-feature euroslots ev">
         <div class="container">
            <div class="row">
               <div class="col-sm-12">
                 <span class="flag-e sk">OCR <strong>Best Casinos in UK</strong></span>
               </div>
             </div>
             <div class="row">
               <div class="col-sm-7">
                  <div class="jackpot-slider">
                      <div id="evobet2" class="owl-carousel">
                         
                         <div>
                           <div class="euro-div">
                             <img src={evobet} />
                             <div class="euro-text">
                             Evo Bet Home Page</div>
                           </div>
                         </div>
                         <div>
                           <div class="euro-div">
                             <img src={evobet} />
                             <div class="euro-text">
                             Evo Bet Home Page</div>
                           </div>
                         </div>
                         <div>
                           <div class="euro-div">
                             <img src={evobet} />
                             <div class="euro-text">
                             Evo Bet Home Page</div>
                           </div>
                         </div>
                         <div>
                           <div class="euro-div">
                             <img src={evobet} />
                             <div class="euro-text">
                             Evo Bet Home Page</div>
                           </div>
                         </div>

                      </div>
                 </div>
               </div>
               <div class="col-sm-5">
                 <div class="jackpot-img">
                   <img src={evo}/>
                 </div>
                 <p>
                     Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing.
                   </p>

                   <ul class="btn-euro">
                     <li><a href="" class="jack-bt">Evo Bet Full Review</a></li>
                     <li><a href="" class="play-now">Play-Now</a></li>
                   </ul>

               </div>
             </div>
           </div>


       </section>

       <section class="section-newest aussie">
         <div class="container">
           <div class="row">
             <div class="col-sm-12">
                <span class="flag-e sk">OCR <strong>Featured Bonus</strong></span>
             </div>
           </div>
           <div class="row">
             <div class="col-sm-3">
               <div class="img-newest">
                 <img src={aussie}/>
               </div>
             </div>
             <div class="col-sm-5">
               <div class="text-newest">
                 <h5>Aussie Play Casino</h5>
                 <h1>150% Up to E120</h1>
                 <p>
                   Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took.
                 </p>
               </div>
             </div>
             <div class="col-sm-4">
                 <ul class="btn-bbo">
                   <li>
                     <a href="#" class="bbo-bt">Aussie Play Full Review</a>
                   </li>
                   <li>
                     <a href="#" class="play-now">Get Bonus</a>
                   </li>
                 </ul>
             </div>
           </div>
         </div>
       </section>
       <section class="section-advertise">
         <div class="row">
           <div class="col-sm-12 text-center">
             <a href="#">
               <div class="ad-image">
                 <img src={addBanner}/>
               </div>
             </a>
           </div>
         </div>
       </section>
       <section class="section-top">
        <div class="container">
          <div class="row">
            <div class="col-sm-8">
               <span class="flag-e sk wh">OCR <strong>News</strong></span>
               <p>
                 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
               </p>
            </div>
             <div class="col-sm-4">
               <a href="#">View all News</a>
            </div>
          </div>
        </div>
       </section>
       <section class="section-news">
        <div class="container">
          <div class="row">
            <div class="col-sm-4">
              <div class="news-card">
                 <div class="news-image">
                   <img src={ns1}/>
                 </div>
                 <h4>LCB News report October 7th - October 13th 2019</h4>
                 <span>
                   <img src={msg}/>&nbsp;0
                 </span>
                 <span class="latest">
                   <img src={latest}/>&nbsp;By Latest Casino
                 </span>
                  <span class="duration">
                   <img src={pen}/>&nbsp;October 14th, 2019.
                 </span>


              </div>
            </div>
             <div class="col-sm-4">
              <div class="news-card">
                 <div class="news-image">
                   <img src={ns2}/>
                 </div>
                 <h4>
                   Elk Grove Casino Close to Realization: Anti Gambling Group 'Stand For California'
                 </h4>
                 <span>
                   <img src={msg}/>&nbsp;0
                 </span>
                 <span class="latest">
                   <img src={womania}/>&nbsp;By Tamara Tam
                 </span>
                  <span class="duration">
                   <img src={pen}/>&nbsp;October 14th, 2019.
                 </span>


              </div>
            </div>
             <div class="col-sm-4">
              <div class="news-card">
                 <div class="news-image">
                   <img src={ns3}/>
                 </div>
                 <h4>Mohegan Gaming Reveals Athens Casino Plans</h4>
                 <span>
                   <img src={msg}/>&nbsp;0
                 </span>
                 <span class="latest">
                   <img src={womania}/>&nbsp;By Tamara Tam
                 </span>
                  <span class="duration">
                   <img src={pen}/>&nbsp;October 14th, 2019.
                 </span>


              </div>
            </div>
             <div class="col-sm-4">
              <div class="news-card">
                 <div class="news-image">
                   <img src={ns4}/>
                 </div>
                 <h4>MGM Resorts Strenthens its Changes for Japan IR</h4>
                 <span>
                   <img src={msg}/>&nbsp;0
                 </span>
                 <span class="latest">
                   <img src={latest}/>&nbsp;By Tamara Tam
                 </span>
                  <span class="duration">
                   <img src={pen}/>&nbsp;October 14th, 2019.
                 </span>


              </div>
            </div>
             <div class="col-sm-4">
              <div class="news-card">
                 <div class="news-image">
                   <img src={ns5}/>
                 </div>
                 <h4>Chicagoland Close to Development of Another Suburban Land-Based Casino Venue</h4>
                 <span>
                   <img src={msg}/>&nbsp;0
                 </span>
                 <span class="latest">
                   <img src={latest}/>&nbsp;By Tamara Tam
                 </span>
                  <span class="duration">
                   <img src={pen}/>&nbsp;October 14th, 2019.
                 </span>


              </div>
            </div>
             <div class="col-sm-4">
              <div class="news-card">
                 <div class="news-image">
                   <img src={ns6}/>
                 </div>
                 <h4>LeoVegas Obtains Gaming Licence For Swedish Market</h4>
                 <span>
                   <img src={msg}/>&nbsp;0
                 </span>
                 <span class="latest">
                   <img src={latest}/>&nbsp;By Tamara Tam
                 </span>
                  <span class="duration">
                   <img src={pen}/>&nbsp;October 14th, 2019.
                 </span>


              </div>
            </div>
          </div>
        </div>
       </section>

       <section class="section-forums">
        
          <div class="row">
            <div class="col-sm-6 null-r">
               <div class="forum">
                   <div class="row">
                     <div class="col-sm-7">
                       <p>
                         Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic.
                       </p>
                     </div>
                     <div class="col-sm-5">
                       <span class="flag-e sk wh">OCR <strong>Casino Forums</strong></span>

                       <div class="forum-image">
                         <img src={forum}/>
                       </div>
                       <a href="#" class="btn-forum">Join OCR Forums</a>
                     </div>
                   </div>
               </div>
            </div>
            <div class="col-sm-6 null-l">
               <div class="forum complain">
                  <div class="row">
                     
                     <div class="col-sm-5">
                       <span class="flag-e sk wh">OCR <strong>Casino Forums</strong></span>

                       <div class="forum-image">
                         <img src={forum2}/>
                       </div>
                       <a href="#" class="btn-forum">Join OCR Forums</a>
                     </div>
                     <div class="col-sm-7">
                       <p>
                         Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic.
                       </p>
                     </div>

                   </div>
               </div>
            </div>
          </div>
      
       </section>

       <section class="section-join">
         <div class="container">
           <div class="row">
             <div class="col-sm-5">
                 <div class="Join-img">
                   <img src={join}/>
                 </div>
             </div>
             <div class="col-sm-7">
                 <div class="Join-logo">
                   <a href="#">
                   <img src={joinLogo}/>
                   </a>
                 </div>
                 <div class="l-logo">OnlineCasinoReview.com</div>
                 <h5>Join today and start earning rewards</h5>
                 <div class="join-form">
                   <form>
                     <input type="text" name="name" placeholder="Enter your name" class="inp"/>
                     <input type="email" name="email" placeholder="Enter your email address" class="inp mail"/>
                     <input type="submit" name="sub" value="Join OCR" class="submit"/>
                   </form>
                 </div>
                 <p>You will get immediately full access to our online casino forum/Chat plus recieve our newsletter with news & Excusive bonuses every month.</p>
             </div>
           </div>
         </div>
       </section>

       <section class="section-join-instant">
           <div class="container">
             <div class="row">
               <div class="col-sm-8 text-center offset-2">
                 <h6>Join instantly with your social account</h6>
                 <ul>
                   <li><a href="#"><div class="social-div"><img src={social1}/></div></a></li>
                   <li><a href="#"><div class="social-div"><img src={social2}/></div></a></li>
                   <li><a href="#"><div class="social-div"><img src={social3}/></div></a></li>
                 </ul>
                 <p>
                   Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently.

                 </p>
               </div>
             </div>
           </div>
       </section>

       <section class="section-advertise">
       <div class="row">
         <div class="col-sm-12 text-center">
           <a href="#">
             <div class="ad-image">
               <img src={addBanner}/>
             </div>
           </a>
         </div>
       </div>
       </section>

 </div>
 <FooterPage {...this.props}></FooterPage>
      
   
 </div>
    )
    return (
      <div className="App" ref={el => (this.div = el)}>
        {this.state.alert}
        <div>{form}</div>
        

        
        </div>
    )
  }
}

export default HomePageComponent;